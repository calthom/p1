Caledonia Thomson 17711136 b9b9
Dexter Chiu 80697148 b4d0b

By submitting this file, I hereby declare that I (or our team of two) worked individually on this assignment
and wrote all of this code. I/we have listed all external resoures (web pages, books) used below. I/we have
listed all people with whom I/we have had significant discussions about the project below.

We discussed this assignment with Hannah Woods and Jillian Tsai

We have successfully implemented all of the body parts:
-Head, Nose
-22 Nose Tentacles (11 on each side, 9 large and 2 small)
-4 Limbs: each with 1 Paw, 5 Claws (4 pts per limb)
-Tail

We have successfully implemented all animations as per their specifications:
- Body tilt up ('u')
- Body tilt down ('e')
- Head move right ('h')
- Head move left ('g')
- Tail move right ('t')
- Tail move left ('v')
- Nose tentacles fan out ('n')
- Swim ('s')
- Dig ('d')

We implemented all the jumpcuts

The only thigns we do not implement were:
	a) simultaneous animations (twisting head while pivoting upwards)
	b) some smooth transitioning/interactivity between different animation affecting the same body segments

This is because the original design we started with was not organized enough and the design patterns did not reveal
themselves to us until later in the project. We spent time refactoring the areas that seemed the most sensible but
didn't have time to refactor everything. Next time we will begin the project thinking more about what kind of global
functions and data structures we should be using to make the project easier for ourselves and make our code more
concise and effective.