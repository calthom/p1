// UBC CPSC 314 (2015W2) -- P1
// HAVE FUN!!! :)

// ASSIGNMENT-SPECIFIC API EXTENSION
THREE.Object3D.prototype.setMatrix = function(a) {
  this.matrix=a;
  this.matrix.decompose(this.position,this.quaternion,this.scale);
}

// SETUP RENDERER & SCENE
var canvas = document.getElementById('canvas');
var scene = new THREE.Scene();
var renderer = new THREE.WebGLRenderer();
renderer.setClearColor(0xFFFFFF); // white background colour
canvas.appendChild(renderer.domElement);

// SETUP CAMERA
var camera = new THREE.PerspectiveCamera(30,1,0.1,1000); // view angle, aspect ratio, near, far
camera.position.set(45,20,40);
camera.lookAt(scene.position);
scene.add(camera);

// SETUP ORBIT CONTROLS OF THE CAMERA
var controls = new THREE.OrbitControls(camera);

// ADAPT TO WINDOW RESIZE
function resize() {
  renderer.setSize(window.innerWidth,window.innerHeight);
  camera.aspect = window.innerWidth/window.innerHeight;
  camera.updateProjectionMatrix();
}

// EVENT LISTENER RESIZE
window.addEventListener('resize',resize);
resize();

//SCROLLBAR FUNCTION DISABLE
window.onscroll = function () {
     window.scrollTo(0,0);
   }

// SETUP HELPER GRID
// Note: Press Z to show/hide
var gridGeometry = new THREE.Geometry();
var i;
for(i=-50;i<51;i+=2) {
    gridGeometry.vertices.push( new THREE.Vector3(i,0,-50));
    gridGeometry.vertices.push( new THREE.Vector3(i,0,50));
    gridGeometry.vertices.push( new THREE.Vector3(-50,0,i));
    gridGeometry.vertices.push( new THREE.Vector3(50,0,i));
}

var gridMaterial = new THREE.LineBasicMaterial({color:0xBBBBBB});
var grid = new THREE.Line(gridGeometry,gridMaterial,THREE.LinePieces);

/////////////////////////////////
//   YOUR WORK STARTS BELOW    //
/////////////////////////////////

// MATERIALS
// Note: Feel free to be creative with this!
var normalMaterial = new THREE.MeshNormalMaterial();

// function drawCube()
// Draws a unit cube centered about the origin.
// Note: You will be using this for all of your geometry
function makeCube() {
  var unitCube = new THREE.BoxGeometry(1,1,1);
  return unitCube;
}

// GEOMETRY
var torsoGeometry = makeCube();
var non_uniform_scale = new THREE.Matrix4().set(5,0,0,0, 0,5,0,0, 0,0,5,0, 0,0,0,1);
torsoGeometry.applyMatrix(non_uniform_scale);

// TO-DO: SPECIFY THE REST OF YOUR STAR-NOSE MOLE'S GEOMETRY.
// Note: You will be using transformation matrices to set the shape.
// Note: You are not allowed to use the tools Three.js provides for
//       rotation, translation and scaling.
// Note: The torso has been done for you (but feel free to modify it!)
// Hint: Explicity declare new matrices using Matrix4().set

var shoulderGeometry = makeCube();
var shoulder_scale = new THREE.Matrix4().set(4.75,0,0,0, 0,4.75,0,0, 0,0,5,0, 0,0,0,1);
shoulderGeometry.applyMatrix(shoulder_scale);

var head0Geometry = makeCube();
var head0_scale = new THREE.Matrix4().set(4,0,0,0, 0,4,0,0, 0,0,4,0, 0,0,0,1);
head0Geometry.applyMatrix(head0_scale);

var head1Geometry = makeCube();
var head1_scale = new THREE.Matrix4().set(3,0,0,0, 0,3,0,0, 0,0,4,0, 0,0,0,1);
head1Geometry.applyMatrix(head1_scale);

var head2Geometry = makeCube();
var head2_scale = new THREE.Matrix4().set(2,0,0,0, 0,2,0,0, 0,0,4,0, 0,0,0,1);
head2Geometry.applyMatrix(head2_scale);

var head3Geometry = makeCube();
var head3_scale = new THREE.Matrix4().set(1,0,0,0, 0,1,0,0, 0,0,4,0, 0,0,0,1);
head3Geometry.applyMatrix(head3_scale);

var short_nose_segment_scale = new THREE.Matrix4().set(0.15,0,0,0, 0,.15,0,0, 0,0,1.5,0, 0,0,0,1);
var shortNoseGeometry = makeCube();
shortNoseGeometry.applyMatrix(short_nose_segment_scale);

var nose_segment_scale = new THREE.Matrix4().set(0.2,0,0,0, 0,.2,0,0, 0,0,3,0, 0,0,0,1);
var noseGeometry = makeCube();
noseGeometry.applyMatrix(nose_segment_scale);

var back0_scale = new THREE.Matrix4().set(4.5,0,0,0, 0,4.5,0,0, 0,0,4,0, 0,0,0,1);
var back0Geometry = makeCube();
back0Geometry.applyMatrix(back0_scale);

var back1_scale = new THREE.Matrix4().set(4,0,0,0, 0,4,0,0, 0,0,3,0, 0,0,0,1);
var back1Geometry = makeCube();
back1Geometry.applyMatrix(back1_scale);

var back2_scale = new THREE.Matrix4().set(2,0,0,0, 0,2,0,0, 0,0,3,0, 0,0,0,1);
var back2Geometry = makeCube();
back2Geometry.applyMatrix(back2_scale);

var tail_segment_scale = new THREE.Matrix4().set(1,0,0,0, 0,1,0,0, 0,0,5,0, 0,0,0,1);
var tailGeometry = makeCube();
tailGeometry.applyMatrix(tail_segment_scale);

var hindleg0_scale = new THREE.Matrix4().set(1,0,0,0, 0,2,0,0, 0,0,1,0, 0,0,0,1);

var hindleg0LeftGeometry = makeCube();
hindleg0LeftGeometry.applyMatrix(hindleg0_scale);

var hindleg0RightGeometry = makeCube();
hindleg0RightGeometry.applyMatrix(hindleg0_scale);

var hindleg1_scale = new THREE.Matrix4().set(1.3,0,0,0, 0,0.4,0,0, 0,0,1.6,0, 0,0,0,1);

var hindleg1LeftGeometry = makeCube();
hindleg1LeftGeometry.applyMatrix(hindleg1_scale);

var hindleg1RightGeometry = makeCube();
hindleg1RightGeometry.applyMatrix(hindleg1_scale);

var hindleg2_scale = new THREE.Matrix4().set(2,0,0,0, 0,0.4,0,0, 0,0,0.6,0, 0,0,0,1);

var hindleg2LeftGeometry = makeCube();
hindleg2LeftGeometry.applyMatrix(hindleg2_scale);

var hindleg2RightGeometry = makeCube();
hindleg2RightGeometry.applyMatrix(hindleg2_scale);

var hindclaw_scale = new THREE.Matrix4().set(0.2,0,0,0, 0,0.2,0,0, 0,0,1,0, 0,0,0,1);
var hindclawGeometry = makeCube();
hindclawGeometry.applyMatrix(hindclaw_scale);

var frontleg0_scale = new THREE.Matrix4().set(1.5,0,0,0, 0,3,0,0, 0,0,1.5,0, 0,0,0,1);

var frontleg0LeftGeometry = makeCube();
frontleg0LeftGeometry.applyMatrix(frontleg0_scale);

var frontleg0RightGeometry = makeCube();
frontleg0RightGeometry.applyMatrix(frontleg0_scale);

var frontleg1_scale = new THREE.Matrix4().set(1.8,0,0,0, 0,0.6,0,0, 0,0,1.6,0, 0,0,0,1);

var frontleg1LeftGeometry = makeCube();
frontleg1LeftGeometry.applyMatrix(frontleg1_scale);

var frontleg1RightGeometry = makeCube();
frontleg1RightGeometry.applyMatrix(frontleg1_scale);

var frontleg2_scale = new THREE.Matrix4().set(2.4,0,0,0, 0,0.6,0,0, 0,0,0.8,0, 0,0,0,1);

var frontleg2LeftGeometry = makeCube();
frontleg2LeftGeometry.applyMatrix(frontleg2_scale);

var frontleg2RightGeometry = makeCube();
frontleg2RightGeometry.applyMatrix(frontleg2_scale);

var frontclaw_scale = new THREE.Matrix4().set(0.25,0,0,0, 0,0.25,0,0, 0,0,1.25,0, 0,0,0,1);
var frontclawGeometry = makeCube();
frontclawGeometry.applyMatrix(frontclaw_scale);

// Matrices of the original locations of body parts
var torsoMatrix = new THREE.Matrix4().set(1,0,0,0, 0,1,0,3.5, 0,0,1,0, 0,0,0,1);

// TO-DO: INITIALIZE THE REST OF YOUR MATRICES
// Note: Use of parent attribute is not allowed.
// Hint: Keep hierarchies in mind!
// Hint: Play around with the headTorsoMatrix values, what changes in the render? Why?

// Keep an array containing the current position of 

// Build head and shoulder segment position matrices
var shoulderMatrix = new THREE.Matrix4().set(1,0,0,0, 0,1,0,0, 0,0,1,2, 0,0,0,1);
var shoulderTorsoMatrix = new THREE.Matrix4().multiplyMatrices(shoulderMatrix,torsoMatrix);

var head0Matrix = new THREE.Matrix4().set(1,0,0,0, 0,1,0,0, 0,0,1,2.5, 0,0,0,1);
var head0TorsoMatrix = new THREE.Matrix4().multiplyMatrices(head0Matrix,shoulderTorsoMatrix);

var head1Matrix = new THREE.Matrix4().set(1,0,0,0, 0,1,0,0, 0,0,1,.75, 0,0,0,1);
var head1TorsoMatrix = new THREE.Matrix4().multiplyMatrices(head1Matrix,head0TorsoMatrix);

var head2Matrix = new THREE.Matrix4().set(1,0,0,0, 0,1,0,-0.15, 0,0,1,.75, 0,0,0,1);
var head2TorsoMatrix = new THREE.Matrix4().multiplyMatrices(head2Matrix,head1TorsoMatrix);

var head3Matrix = new THREE.Matrix4().set(1,0,0,0, 0,1,0,-0.30, 0,0,1,.5, 0,0,0,1);
var head3TorsoMatrix = new THREE.Matrix4().multiplyMatrices(head3Matrix,head2TorsoMatrix);

// Build nose segment position matrices
var noseMatrix = [new THREE.Matrix4().set(.5,0,0,.05, 0,.5,0,.4, 0,0,1,2, 0,0,0,1), 
  new THREE.Matrix4().set(.5,0,0,-.05, 0,.5,0,.4, 0,0,1,2, 0,0,0,1),
  new THREE.Matrix4().set(.5,0,0,.15, 0,.5,0,.4, 0,0,1,2, 0,0,0,1),
  new THREE.Matrix4().set(.5,0,0,-.15, 0,.5,0,.4, 0,0,1,2, 0,0,0,1),
  new THREE.Matrix4().set(.5,0,0,.3, 0,.5,0,.4, 0,0,1,2, 0,0,0,1),
  new THREE.Matrix4().set(.5,0,0,-.3, 0,.5,0,.4, 0,0,1,2, 0,0,0,1),
  new THREE.Matrix4().set(.5,0,0,.1, 0,.5,0,-.45, 0,0,1,2, 0,0,0,1),
  new THREE.Matrix4().set(.5,0,0,-.1, 0,.5,0,-.45, 0,0,1,2, 0,0,0,1),
  new THREE.Matrix4().set(.5,0,0,.2, 0,.5,0,-.45, 0,0,1,2, 0,0,0,1),
  new THREE.Matrix4().set(.5,0,0,-.2, 0,.5,0,-.45, 0,0,1,2, 0,0,0,1),
  new THREE.Matrix4().set(.5,0,0,.45, 0,.5,0,-.45, 0,0,1,2, 0,0,0,1),
  new THREE.Matrix4().set(.5,0,0,-.45, 0,.5,0,-.45, 0,0,1,2, 0,0,0,1),
  new THREE.Matrix4().set(.5,0,0,-.45, 0,.5,0,-.3, 0,0,1,2, 0,0,0,1),
  new THREE.Matrix4().set(.5,0,0,-.45, 0,.5,0,-.10, 0,0,1,2, 0,0,0,1),
  new THREE.Matrix4().set(.5,0,0,-.45, 0,.5,0,.05, 0,0,1,2, 0,0,0,1),
  new THREE.Matrix4().set(.5,0,0,-.45, 0,.5,0,.2, 0,0,1,2, 0,0,0,1),
  new THREE.Matrix4().set(.5,0,0,-.45, 0,.5,0,.45, 0,0,1,2, 0,0,0,1),
  new THREE.Matrix4().set(.5,0,0,.45, 0,.5,0,-.3, 0,0,1,2, 0,0,0,1),
  new THREE.Matrix4().set(.5,0,0,.45, 0,.5,0,-.10, 0,0,1,2, 0,0,0,1),
  new THREE.Matrix4().set(.5,0,0,.45, 0,.5,0,.05, 0,0,1,2, 0,0,0,1),
  new THREE.Matrix4().set(.5,0,0,.45, 0,.5,0,.2, 0,0,1,2, 0,0,0,1),
  new THREE.Matrix4().set(.5,0,0,.45, 0,.5,0,.45, 0,0,1,2, 0,0,0,1)]

// Store position in respect to hierarchy of all nose segments 
var noseTorsoMatrix = [];
for (i = 0; i < 22; i++) {
    noseTorsoMatrix[i] = new THREE.Matrix4().multiplyMatrices(head3TorsoMatrix, noseMatrix[i]);
}

// Build back and tail segment position matrices
var back0Matrix = new THREE.Matrix4().set(1,0,0,0, 0,1,0,0, 0,0,1,-1, 0,0,0,1);
var back0TorsoMatrix = new THREE.Matrix4().multiplyMatrices(back0Matrix,torsoMatrix);

var back1Matrix = new THREE.Matrix4().set(1,0,0,0, 0,1,0,-0.2, 0,0,1,-1, 0,0,0,1);
var back1TorsoMatrix = new THREE.Matrix4().multiplyMatrices(back1Matrix,back0TorsoMatrix);

var back2Matrix = new THREE.Matrix4().set(1,0,0,0, 0,1,0,-0.9, 0,0,1,-0.5, 0,0,0,1);
var back2TorsoMatrix = new THREE.Matrix4().multiplyMatrices(back2Matrix,back1TorsoMatrix);

var tailMatrix = new THREE.Matrix4().set(1,0,0,0, 0,1,0,-0.3, 0,0,1,-4, 0,0,0,1);
var tailTorsoMatrix = new THREE.Matrix4().multiplyMatrices(back2TorsoMatrix,tailMatrix);

// Build hind leg segment position matrices
var hindleg0LeftMatrix = new THREE.Matrix4().set(1,0,0,-2.1, 0,1,0,-2, 0,0,1,-1, 0,0,0,1);
var hindleg0LeftTorsoMatrix = new THREE.Matrix4().multiplyMatrices(back0TorsoMatrix,hindleg0LeftMatrix);

var hindleg0RightMatrix = new THREE.Matrix4().set(1,0,0,2.1, 0,1,0,-2, 0,0,1,-1, 0,0,0,1);
var hindleg0RightTorsoMatrix = new THREE.Matrix4().multiplyMatrices(back0TorsoMatrix,hindleg0RightMatrix);

var hindleg1LeftMatrix = new THREE.Matrix4().set(1,0,0,0, 0,1,0,-1.2, 0,0,1,0.5, 0,0,0,1);
var hindleg1LeftTorsoMatrix = new THREE.Matrix4().multiplyMatrices(hindleg0LeftTorsoMatrix,hindleg1LeftMatrix);

var hindleg1RightMatrix = new THREE.Matrix4().set(1,0,0,0, 0,1,0,-1.2, 0,0,1,0.5, 0,0,0,1);
var hindleg1RightTorsoMatrix = new THREE.Matrix4().multiplyMatrices(hindleg0RightTorsoMatrix,hindleg1RightMatrix);

var hindleg2LeftMatrix = new THREE.Matrix4().set(1,0,0,0, 0,1,0,0, 0,0,1,0.7, 0,0,0,1);
var hindleg2LeftTorsoMatrix = new THREE.Matrix4().multiplyMatrices(hindleg1LeftTorsoMatrix,hindleg2LeftMatrix);

var hindleg2RightMatrix = new THREE.Matrix4().set(1,0,0,0, 0,1,0,0, 0,0,1,0.7, 0,0,0,1);
var hindleg2RightTorsoMatrix = new THREE.Matrix4().multiplyMatrices(hindleg1RightTorsoMatrix,hindleg2RightMatrix);

// Build front leg segment position matrices
var frontleg0LeftMatrix = new THREE.Matrix4().set(1,0,0,-2.1, 0,1,0,-1.83, 0,0,1,1, 0,0,0,1);
var frontleg0LeftTorsoMatrix = new THREE.Matrix4().multiplyMatrices(shoulderTorsoMatrix,frontleg0LeftMatrix);

var frontleg0RightMatrix = new THREE.Matrix4().set(1,0,0,2.1, 0,1,0,-1.83, 0,0,1,1, 0,0,0,1);
var frontleg0RightTorsoMatrix = new THREE.Matrix4().multiplyMatrices(shoulderTorsoMatrix,frontleg0RightMatrix);

var frontleg1LeftMatrix = new THREE.Matrix4().set(1,0,0,0, 0,1,0,-1.2, 0,0,1,0.5, 0,0,0,1);
var frontleg1LeftTorsoMatrix = new THREE.Matrix4().multiplyMatrices(frontleg0LeftTorsoMatrix,frontleg1LeftMatrix);

var frontleg1RightMatrix = new THREE.Matrix4().set(1,0,0,0, 0,1,0,-1.2, 0,0,1,0.5, 0,0,0,1);
var frontleg1RightTorsoMatrix = new THREE.Matrix4().multiplyMatrices(frontleg0RightTorsoMatrix,frontleg1RightMatrix);

var frontleg2LeftMatrix = new THREE.Matrix4().set(1,0,0,0, 0,1,0,0, 0,0,1,0.7, 0,0,0,1);
var frontleg2LeftTorsoMatrix = new THREE.Matrix4().multiplyMatrices(frontleg1LeftTorsoMatrix,frontleg2LeftMatrix);

var frontleg2RightMatrix = new THREE.Matrix4().set(1,0,0,0, 0,1,0,0, 0,0,1,0.7, 0,0,0,1);
var frontleg2RightTorsoMatrix = new THREE.Matrix4().multiplyMatrices(frontleg1RightTorsoMatrix,frontleg2RightMatrix);

// Build claw segment position matrices
// 0 - 4 left hind claws
// 5 - 9 right hind claws
// 10 - 14 left front claws
// 15 - 19 right front
var clawMatrix = [new THREE.Matrix4().set(1,0,0,0, 0,1,0,-.1, 0,0,1,0.7, 0,0,0,1),
  new THREE.Matrix4().set(1,0,0,-0.4, 0,1,0,-.1, 0,0,1,0.7, 0,0,0,1),
  new THREE.Matrix4().set(1,0,0,0.4, 0,1,0,-.1, 0,0,1,0.7, 0,0,0,1),
  new THREE.Matrix4().set(1,0,0,-0.8, 0,1,0,-.1, 0,0,1,0.7, 0,0,0,1),
  new THREE.Matrix4().set(1,0,0,0.8, 0,1,0,-.1, 0,0,1,0.7, 0,0,0,1),
  new THREE.Matrix4().set(1,0,0,0, 0,1,0,-.1, 0,0,1,0.7, 0,0,0,1),
  new THREE.Matrix4().set(1,0,0,-0.4, 0,1,0,-.1, 0,0,1,0.7, 0,0,0,1),
  new THREE.Matrix4().set(1,0,0,0.4, 0,1,0,-.1, 0,0,1,0.7, 0,0,0,1),
  new THREE.Matrix4().set(1,0,0,-0.8, 0,1,0,-.1, 0,0,1,0.7, 0,0,0,1),
  new THREE.Matrix4().set(1,0,0,0.8, 0,1,0,-.1, 0,0,1,0.7, 0,0,0,1),
  new THREE.Matrix4().set(1,0,0,0, 0,1,0,-.1, 0,0,1,0.7, 0,0,0,1),
  new THREE.Matrix4().set(1,0,0,-0.5, 0,1,0,-.1, 0,0,1,0.7, 0,0,0,1),
  new THREE.Matrix4().set(1,0,0,0.5, 0,1,0,-.1, 0,0,1,0.7, 0,0,0,1),
  new THREE.Matrix4().set(1,0,0,-1, 0,1,0,-.1, 0,0,1,0.7, 0,0,0,1),
  new THREE.Matrix4().set(1,0,0,1, 0,1,0,-.1, 0,0,1,0.7, 0,0,0,1),
  new THREE.Matrix4().set(1,0,0,0, 0,1,0,-.1, 0,0,1,0.7, 0,0,0,1),
  new THREE.Matrix4().set(1,0,0,-0.5, 0,1,0,-.1, 0,0,1,0.7, 0,0,0,1),
  new THREE.Matrix4().set(1,0,0,0.5, 0,1,0,-.1, 0,0,1,0.7, 0,0,0,1),
  new THREE.Matrix4().set(1,0,0,-1, 0,1,0,-.1, 0,0,1,0.7, 0,0,0,1),
  new THREE.Matrix4().set(1,0,0,1, 0,1,0,-.1, 0,0,1,0.7, 0,0,0,1)]

// Store original position of claw segments in scene and the current location of all claw segments
// Create position matrix arrays for left back claws
var clawTorsoMatrix = [];
for (i = 0; i < 20; i++) {

  var parentTorsoMatrix = hindleg2LeftTorsoMatrix;

  // Set parent matrix based on where the claw is
  // 0 - 4 left hind claws
  // 5 - 9 right hind claws
  // 10 - 14 left front claws
  // 15 - 19 right front
  switch (Math.floor(i / 5)) {
    case 0:
        parentTorsoMatrix = hindleg2LeftTorsoMatrix;
        break;
    case 1:
        parentTorsoMatrix = hindleg2RightTorsoMatrix;
        break;
    case 2:
        parentTorsoMatrix = frontleg2LeftTorsoMatrix;
        break;
    case 3:
        parentTorsoMatrix = frontleg2RightTorsoMatrix;
        break;
    }

    clawTorsoMatrix[i] = new THREE.Matrix4().multiplyMatrices(parentTorsoMatrix, clawMatrix[i]);
}

// CREATE BODY
var torso = new THREE.Mesh(torsoGeometry,normalMaterial);
torso.setMatrix(torsoMatrix)
scene.add(torso);

// TO-DO: PUT TOGETHER THE REST OF YOUR STAR-NOSED MOLE AND ADD TO THE SCENE!
// Hint: Hint: Add one piece of geometry at a time, then implement the motion for that part.
//             Then you can make sure your hierarchy still works properly after each step.

// CREATE SHOULDERS
var shoulder = new THREE.Mesh(shoulderGeometry,normalMaterial);
shoulder.setMatrix(shoulderTorsoMatrix)
scene.add(shoulder);

// CREATE HEAD
var head0 = new THREE.Mesh(head0Geometry,normalMaterial);
head0.setMatrix(head0TorsoMatrix)
scene.add(head0);

var head1 = new THREE.Mesh(head1Geometry,normalMaterial);
head1.setMatrix(head1TorsoMatrix)
scene.add(head1);

var head2 = new THREE.Mesh(head2Geometry,normalMaterial);
head2.setMatrix(head2TorsoMatrix)
scene.add(head2);

var head3 = new THREE.Mesh(head3Geometry,normalMaterial);
head3.setMatrix(head3TorsoMatrix)
scene.add(head3);

// CREATE NOSE
var nose = []

// Create short nose segments on top
for(i = 0; i < 2; i++){
  nose[i] = new THREE.Mesh(shortNoseGeometry,normalMaterial)
  nose[i].setMatrix(noseTorsoMatrix[i])
  scene.add(nose[i]);
}

// Create long nose segments on top
for(i = 2; i < 6; i++){
  nose[i] = new THREE.Mesh(noseGeometry,normalMaterial)
  nose[i].setMatrix(noseTorsoMatrix[i])
  scene.add(nose[i]);
}

// Create short nose segments on bottom
for(i = 6; i < 8; i++){
  nose[i] = new THREE.Mesh(shortNoseGeometry,normalMaterial)
  nose[i].setMatrix(noseTorsoMatrix[i])
  scene.add(nose[i]);
}

// Create long nose segments on bottom, left and right side
for(i = 8; i < 22; i++){
  nose[i] = new THREE.Mesh(noseGeometry,normalMaterial)
  nose[i].setMatrix(noseTorsoMatrix[i])
  scene.add(nose[i]);
}

// CREATE BACK SLICE 0
var back0 = new THREE.Mesh(back0Geometry,normalMaterial);
back0.setMatrix(back0TorsoMatrix)
scene.add(back0);

// CREATE BACK SLICE 1
var back1 = new THREE.Mesh(back1Geometry,normalMaterial);
back1.setMatrix(back1TorsoMatrix)
scene.add(back1);

// CREATE BACK SLICE 2
var back2 = new THREE.Mesh(back2Geometry,normalMaterial);
back2.setMatrix(back2TorsoMatrix)
scene.add(back2);

// CREATE TAIL
var tail = new THREE.Mesh(tailGeometry,normalMaterial);
tail.setMatrix(tailTorsoMatrix)
scene.add(tail);

// CREATE HIND LEG
var hindleg0Left = new THREE.Mesh(hindleg0LeftGeometry,normalMaterial);
hindleg0Left.setMatrix(hindleg0LeftTorsoMatrix)
scene.add(hindleg0Left);

var hindleg0Right = new THREE.Mesh(hindleg0RightGeometry,normalMaterial);
hindleg0Right.setMatrix(hindleg0RightTorsoMatrix)
scene.add(hindleg0Right);

var hindleg1Left = new THREE.Mesh(hindleg1LeftGeometry,normalMaterial);
hindleg1Left.setMatrix(hindleg1LeftTorsoMatrix)
scene.add(hindleg1Left);

var hindleg1Right = new THREE.Mesh(hindleg1RightGeometry,normalMaterial);
hindleg1Right.setMatrix(hindleg1RightTorsoMatrix)
scene.add(hindleg1Right);

var hindleg2Left = new THREE.Mesh(hindleg2LeftGeometry,normalMaterial);
hindleg2Left.setMatrix(hindleg2LeftTorsoMatrix)
scene.add(hindleg2Left);

var hindleg2Right = new THREE.Mesh(hindleg2RightGeometry,normalMaterial);
hindleg2Right.setMatrix(hindleg2RightTorsoMatrix)
scene.add(hindleg2Right);

// CREATE front LEG
var frontleg0Left = new THREE.Mesh(frontleg0LeftGeometry,normalMaterial);
frontleg0Left.setMatrix(frontleg0LeftTorsoMatrix)
scene.add(frontleg0Left);

var frontleg0Right = new THREE.Mesh(frontleg0RightGeometry,normalMaterial);
frontleg0Right.setMatrix(frontleg0RightTorsoMatrix)
scene.add(frontleg0Right);

var frontleg1Left = new THREE.Mesh(frontleg1LeftGeometry,normalMaterial);
frontleg1Left.setMatrix(frontleg1LeftTorsoMatrix)
scene.add(frontleg1Left);

var frontleg1Right = new THREE.Mesh(frontleg1RightGeometry,normalMaterial);
frontleg1Right.setMatrix(frontleg1RightTorsoMatrix)
scene.add(frontleg1Right);

var frontleg2Left = new THREE.Mesh(frontleg2LeftGeometry,normalMaterial);
frontleg2Left.setMatrix(frontleg2LeftTorsoMatrix)
scene.add(frontleg2Left);

var frontleg2Right = new THREE.Mesh(frontleg2RightGeometry,normalMaterial);
frontleg2Right.setMatrix(frontleg2RightTorsoMatrix)
scene.add(frontleg2Right);

// CREATE NOSE
var claw = []

// Create hind claw segments
for(i = 0; i < 10; i++){
  claw[i] = new THREE.Mesh(hindclawGeometry,normalMaterial);
  claw[i].setMatrix(clawTorsoMatrix[i])
  scene.add(claw[i]);
}

// Create front claw segments
for(i = 10; i < 20; i++){
  claw[i] = new THREE.Mesh(frontclawGeometry,normalMaterial)
  claw[i].setMatrix(clawTorsoMatrix[i])
  scene.add(claw[i]);
}

// APPLY DIFFERENT JUMP CUTS/ANIMATIONS TO DIFFERNET KEYS
// Note: The start of "U" animation has been done for you, you must implement the hiearchy and jumpcut.
// Hint: There are other ways to manipulate and grab clock values!!
// Hint: Check THREE.js clock documenation for ideas.
// Hint: It may help to start with a jumpcut and implement the animation after.
// Hint: Where is updateBody() called?
var clock = new THREE.Clock(true);

var p0; // start position or angle
var p1; // end position or angle
var time_length; // total time of animation
var time_start; // start time of animation
var time_end; // end time of animation
var p; // current frame
var animate = false; // animate?
var animate_flag = true; // keyboard flag
var swim_phase = 0; // swimming flag to keep track of swimming progress

// function init_animation()
// Initializes parameters and sets animate flag to true.
// Input: start position or angle, end position or angle, and total time of animation.
function init_animation(p_start,p_end,t_length){
  p0 = p_start;
  p1 = p_end;
  time_length = t_length;
  time_start = clock.getElapsedTime();
  time_end = time_start + time_length;
  animate = true; // flag for animation
  return;
}

newNosePosition = [];

function updateBody() {
  switch(true)
  {
      case((key == "E" || key == "U")):
      var time = clock.getElapsedTime(); // t seconds passed since the clock started.

      if (time > time_end){
        p = p1;
        animate = false;
        break;
      }

      if (animate_flag){
          p = (p1 - p0)*((time-time_start)/time_length) + p0; // current frame
      } else {
          p = p1;
      }

      var rotateZ;

      if(key == "U") {
        rotateZ = new THREE.Matrix4().set(1,        0,         0,        0,
                                      0, Math.cos(-p),-Math.sin(-p), 0,
                                      0, Math.sin(-p), Math.cos(-p), 0,
                                      0,        0,         0,        1);
      } else {
        rotateZ = new THREE.Matrix4().set(1,        0,         0,        0,
                                      0, Math.cos(-p), Math.sin(-p), 0,
                                      0, -Math.sin(-p), Math.cos(-p), 0,
                                      0,        0,         0,        1);
      }

      var torsoRotMatrix = new THREE.Matrix4().multiplyMatrices(rotateZ,torsoMatrix);
      torso.setMatrix(torsoRotMatrix);
      var shoulderRotMatrix = new THREE.Matrix4().multiplyMatrices(torsoRotMatrix,shoulderMatrix);
      shoulder.setMatrix(shoulderRotMatrix);

      updateHead(rotateZ);

      var back0RotMatrix = new THREE.Matrix4().multiplyMatrices(torsoRotMatrix,back0Matrix);
      back0.setMatrix(back0RotMatrix);
      var back1RotMatrix = new THREE.Matrix4().multiplyMatrices(back0RotMatrix,back1Matrix);
      back1.setMatrix(back1RotMatrix);
      var back2RotMatrix = new THREE.Matrix4().multiplyMatrices(back1RotMatrix,back2Matrix);
      back2.setMatrix(back2RotMatrix);
      var tailRotMatrix = new THREE.Matrix4().multiplyMatrices(back2RotMatrix,tailMatrix);
      tail.setMatrix(tailRotMatrix);

      // HIND LEG ROTATION
      var hindleg0LeftRotMatrix = new THREE.Matrix4().multiplyMatrices(back0RotMatrix,hindleg0LeftMatrix);
      hindleg0Left.setMatrix(hindleg0LeftRotMatrix);
      var hindleg0RightRotMatrix = new THREE.Matrix4().multiplyMatrices(back0RotMatrix,hindleg0RightMatrix);
      hindleg0Right.setMatrix(hindleg0RightRotMatrix);
      var hindleg1LeftRotMatrix = new THREE.Matrix4().multiplyMatrices(hindleg0LeftRotMatrix,hindleg1LeftMatrix);
      hindleg1Left.setMatrix(hindleg1LeftRotMatrix);
      var hindleg1RightRotMatrix = new THREE.Matrix4().multiplyMatrices(hindleg0RightRotMatrix,hindleg1RightMatrix);
      hindleg1Right.setMatrix(hindleg1RightRotMatrix);
      var hindleg2LeftRotMatrix = new THREE.Matrix4().multiplyMatrices(hindleg1LeftRotMatrix,hindleg2LeftMatrix);
      hindleg2Left.setMatrix(hindleg2LeftRotMatrix);
      var hindleg2RightRotMatrix = new THREE.Matrix4().multiplyMatrices(hindleg1RightRotMatrix,hindleg2RightMatrix);
      hindleg2Right.setMatrix(hindleg2RightRotMatrix);

      // HIND CLAW ROTATION
      // Rotate left hind claws
      for(i = 0; i < 5; i++){
        updateSegment(claw[i], hindleg2LeftRotMatrix, clawMatrix[i], clawTorsoMatrix[i])
      }

      // Rotate right hind claws
      for(i = 5; i < 10; i++){
        updateSegment(claw[i], hindleg2RightRotMatrix, clawMatrix[i], clawTorsoMatrix[i])
      }


      // FRONT LEG ROTATION
      var frontleg0LeftRotMatrix = new THREE.Matrix4().multiplyMatrices(shoulderRotMatrix,frontleg0LeftMatrix);
      frontleg0Left.setMatrix(frontleg0LeftRotMatrix);
      var frontleg0RightRotMatrix = new THREE.Matrix4().multiplyMatrices(shoulderRotMatrix,frontleg0RightMatrix);
      frontleg0Right.setMatrix(frontleg0RightRotMatrix);
      var frontleg1LeftRotMatrix = new THREE.Matrix4().multiplyMatrices(frontleg0LeftRotMatrix,frontleg1LeftMatrix);
      frontleg1Left.setMatrix(frontleg1LeftRotMatrix);
      var frontleg1RightRotMatrix = new THREE.Matrix4().multiplyMatrices(frontleg0RightRotMatrix,frontleg1RightMatrix);
      frontleg1Right.setMatrix(frontleg1RightRotMatrix);
      var frontleg2LeftRotMatrix = new THREE.Matrix4().multiplyMatrices(frontleg1LeftRotMatrix,frontleg2LeftMatrix);
      frontleg2Left.setMatrix(frontleg2LeftRotMatrix);
      var frontleg2RightRotMatrix = new THREE.Matrix4().multiplyMatrices(frontleg1RightRotMatrix,frontleg2RightMatrix);
      frontleg2Right.setMatrix(frontleg2RightRotMatrix);

      // FRONT CLAW ROTATION
      // Rotate left front claws
      for(i = 10; i < 15; i++){
        updateSegment(claw[i], frontleg2LeftRotMatrix, clawMatrix[i], clawTorsoMatrix[i])
      }

      // Rotate right front claws
      for(i = 15; i < 20; i++){
        updateSegment(claw[i], frontleg2RightRotMatrix, clawMatrix[i], clawTorsoMatrix[i])
      }

      break;

      // JUMPCUT/ANIMATION FOR EACH KEY!

      case((key == "H" || key == "G")):
      var time = clock.getElapsedTime(); // t seconds passed since the clock started.

      if (time > time_end){
        p = p1;
        animate = false;
        break;
      }

      if (animate_flag){
          p = (p1 - p0)*((time-time_start)/time_length) + p0; // current frame
      } else {
          p = p1;
      }

      var rotateY;

      if(key == "H") {
        rotateY = new THREE.Matrix4().set(Math.cos(-p), 0, Math.sin(-p), 0,
                                      0, 1, 0, 0,
                                      -Math.sin(-p), 0, Math.cos(-p), 0,
                                      0, 0, 0, 1);
        rotateY_half = new THREE.Matrix4().set(Math.cos(-p/2), 0, Math.sin(-p/2), 0,
                                      0, 1, 0, 0,
                                      -Math.sin(-p/2), 0, Math.cos(-p/2), 0,
                                      0, 0, 0, 1);
      } else {
        rotateY = new THREE.Matrix4().set(Math.cos(-p), 0, -Math.sin(-p), 0,
                                      0, 1, 0, 0,
                                      Math.sin(-p), 0, Math.cos(-p), 0,
                                      0, 0, 0, 1);
        rotateY_half = new THREE.Matrix4().set(Math.cos(-p/2), 0, -Math.sin(-p/2), 0,
                                      0, 1, 0, 0,
                                      Math.sin(-p/2), 0, Math.cos(-p/2), 0,
                                      0, 0, 0, 1);
      }

      // Head rotates left or right
      updateHead(rotateY, rotateY_half);

      // Shoulders rotate half the amount of the head
      var shoulderRotMatrix = new THREE.Matrix4().multiplyMatrices(rotateY_half,shoulderTorsoMatrix);
      shoulder.setMatrix(shoulderRotMatrix);

      break;

      case((key == "T" || key == "V")):
      var time = clock.getElapsedTime(); // t seconds passed since the clock started.

      if (time > time_end){
        p = p1;
        animate = false;
        break;
      }

      if (animate_flag){
          p = (p1 - p0)*((time-time_start)/time_length) + p0; // current frame
      } else {
          p = p1;
      }

      var rotateY;

      if(key == "V") {
        rotateY = new THREE.Matrix4().set(Math.cos(-p), 0, Math.sin(-p), 0,
                                      0, 1, 0, 0,
                                      -Math.sin(-p), 0, Math.cos(-p), 0,
                                      0, 0, 0, 1);
        rotateY_tail = new THREE.Matrix4().set(Math.cos(-p*1.5), 0, Math.sin(-p*1.5), 0,
                                      0, 1, 0, 0,
                                      -Math.sin(-p*1.5), 0, Math.cos(-p*1.5), 0,
                                      0, 0, 0, 1);
      } else {
        rotateY = new THREE.Matrix4().set(Math.cos(-p), 0, -Math.sin(-p), 0,
                                      0, 1, 0, 0,
                                      Math.sin(-p), 0, Math.cos(-p), 0,
                                      0, 0, 0, 1);
        rotateY_tail = new THREE.Matrix4().set(Math.cos(-p*1.5), 0, -Math.sin(-p*1.5), 0,
                                      0, 1, 0, 0,
                                      Math.sin(-p*1.5), 0, Math.cos(-p*1.5), 0,
                                      0, 0, 0, 1);
      }

      // back rotates left or right
      var back0RotMatrix = new THREE.Matrix4().multiplyMatrices(rotateY,back0TorsoMatrix);
      back0.setMatrix(back0RotMatrix);
      var back1RotMatrix = new THREE.Matrix4().multiplyMatrices(back0RotMatrix,back1Matrix);
      back1.setMatrix(back1RotMatrix);
      var back2RotMatrix = new THREE.Matrix4().multiplyMatrices(back1RotMatrix,back2Matrix);
      back2.setMatrix(back2RotMatrix);
      // tail rotate more than the back (scale accordingly)
      var tailRotMatrix = new THREE.Matrix4().multiplyMatrices(rotateY_tail,tailTorsoMatrix);
      tail.setMatrix(tailRotMatrix);

      break

      case(key == "D"):
      var time = clock.getElapsedTime(); // t seconds passed since the clock started.

      if (time > time_end){
        p = p1;
        animate = false;
        break;
      }

      if (animate_flag){
          p = (p1 - p0)*((time-time_start)/time_length) + p0; // current frame
      } else {
          p = p1;
      }

      var rotateZ0 = new THREE.Matrix4().set(1,        0,         0,        0,
                                            0, Math.cos(-p/2), -Math.sin(-p/2), 0,
                                            0, Math.sin(-p/2), Math.cos(-p/2), 0,
                                            0,        0,         0,        1);
      var rotateZ1 = new THREE.Matrix4().set(1,        0,         0,        0,
                                            0, Math.cos(-p*2), Math.sin(-p*2), 0,
                                            0, -Math.sin(-p*2), Math.cos(-p*2), 0,
                                            0,        0,         0,        1);
      var rotateZ2 = new THREE.Matrix4().set(1,        0,         0,        0,
                                            0, Math.cos(-p*1.5), Math.sin(-p*1.5), 0,
                                            0, -Math.sin(-p*1.5), Math.cos(-p*1.5), 0,
                                            0,        0,         0,        1);
      var rotateZ3 = new THREE.Matrix4().set(1,        0,         0,        0,
                                            0, Math.cos(-p*4), Math.sin(-p*4), 0,
                                            0, -Math.sin(-p*4), Math.cos(-p*4), 0,
                                            0,        0,         0,        1);
      // FRONT LEG ROTATION
      var frontleg0LeftRotMatrix = new THREE.Matrix4().multiplyMatrices(rotateZ0, frontleg0LeftTorsoMatrix);
      frontleg0Left.setMatrix(frontleg0LeftRotMatrix);
      var frontleg0RightRotMatrix = new THREE.Matrix4().multiplyMatrices(rotateZ0, frontleg0RightTorsoMatrix);
      frontleg0Right.setMatrix(frontleg0RightRotMatrix);
      var tempRotMatrix = new THREE.Matrix4().multiplyMatrices(rotateZ1, frontleg1LeftMatrix);
      var frontleg1LeftRotMatrix = new THREE.Matrix4().multiplyMatrices(frontleg0LeftRotMatrix,tempRotMatrix);
      frontleg1Left.setMatrix(frontleg1LeftRotMatrix);
      tempRotMatrix = new THREE.Matrix4().multiplyMatrices(rotateZ1, frontleg1RightMatrix);
      var frontleg1RightRotMatrix = new THREE.Matrix4().multiplyMatrices(frontleg0RightRotMatrix,tempRotMatrix);
      frontleg1Right.setMatrix(frontleg1RightRotMatrix);

      // all claws connected to leg2, rotate this instead of each individual claw
      tempRotMatrix = new THREE.Matrix4().multiplyMatrices(rotateZ2, frontleg2LeftMatrix);
      var frontleg2LeftRotMatrix = new THREE.Matrix4().multiplyMatrices(frontleg1LeftRotMatrix,tempRotMatrix);
      frontleg2Left.setMatrix(frontleg2LeftRotMatrix);
      tempRotMatrix = new THREE.Matrix4().multiplyMatrices(rotateZ2, frontleg2RightMatrix);
      var frontleg2RightRotMatrix = new THREE.Matrix4().multiplyMatrices(frontleg1RightRotMatrix,tempRotMatrix);
      frontleg2Right.setMatrix(frontleg2RightRotMatrix);

      // Rotate front left claws
      for(i = 10; i < 15; i++){
        updateSegment(claw[i], frontleg2LeftRotMatrix, clawMatrix[i], clawTorsoMatrix[i])
      }

      // Rotate front left claws
      for(i = 15; i < 20; i++){
        updateSegment(claw[i], frontleg2RightRotMatrix, clawMatrix[i], clawTorsoMatrix[i])
      }

      // Curl claws futher than rest
      for(i = 10; i < 20; i++){
        fanNoseSegment(claw[i], rotateZ3, clawMatrix[i], clawTorsoMatrix[i]) 
      }

      break;

      case(key == "S" && swim_phase == 1):
      var time = clock.getElapsedTime(); // t seconds passed since the clock started.

      if (time > time_end){
        p = p1;
        animate = false;
        break;
      }

      if (animate_flag){
          p = (p1 - p0)*((time-time_start)/time_length) + p0; // current frame
      } else {
          p = p1;
      }

      var rotateZ0 = new THREE.Matrix4().set(1,        0,         0,        0,
                                            0, Math.cos(-p/5), -Math.sin(-p/5), 0,
                                            0, Math.sin(-p/5), Math.cos(-p/5), 0,
                                            0,        0,         0,        1);
      var rotateZ1 = new THREE.Matrix4().set(1,        0,         0,        0,
                                            0, Math.cos(-p*2), Math.sin(-p*2), 0,
                                            0, -Math.sin(-p*2), Math.cos(-p*2), 0,
                                            0,        0,         0,        1);
      var rotateZ2 = new THREE.Matrix4().set(1,        0,         0,        0,
                                            0, Math.cos(p/2.5), -Math.sin(p/2.5), 0,
                                            0, Math.sin(p/2.5), Math.cos(p/2.5), 0,
                                            0,        0,         0,        1);
      // FRONT LEG ROTATION
      var hindleg0LeftRotMatrix = new THREE.Matrix4().multiplyMatrices(rotateZ2, hindleg0LeftTorsoMatrix);
      hindleg0Left.setMatrix(hindleg0LeftRotMatrix);
      var frontleg0RightRotMatrix = new THREE.Matrix4().multiplyMatrices(rotateZ0, frontleg0RightTorsoMatrix);
      frontleg0Right.setMatrix(frontleg0RightRotMatrix);
      var tempRotMatrix = new THREE.Matrix4().multiplyMatrices(rotateZ1, hindleg1LeftMatrix);
      var hindleg1LeftRotMatrix = new THREE.Matrix4().multiplyMatrices(hindleg0LeftRotMatrix,hindleg1LeftMatrix);
      hindleg1Left.setMatrix(hindleg1LeftRotMatrix);
      tempRotMatrix = new THREE.Matrix4().multiplyMatrices(rotateZ1, frontleg1RightMatrix);
      var frontleg1RightRotMatrix = new THREE.Matrix4().multiplyMatrices(frontleg0RightRotMatrix,frontleg1RightMatrix);
      frontleg1Right.setMatrix(frontleg1RightRotMatrix);

      // all claws connected to leg2, rotate this instead of each individual claw
      var hindleg2LeftRotMatrix = new THREE.Matrix4().multiplyMatrices(hindleg1LeftRotMatrix,hindleg2LeftMatrix);
      hindleg2Left.setMatrix(hindleg2LeftRotMatrix);
      var frontleg2RightRotMatrix = new THREE.Matrix4().multiplyMatrices(frontleg1RightRotMatrix,frontleg2RightMatrix);
      frontleg2Right.setMatrix(frontleg2RightRotMatrix);

      // Rotate front right claws
      for(i = 15; i < 20; i++){
        updateSegment(claw[i], frontleg2RightRotMatrix, clawMatrix[i], clawTorsoMatrix[i])
      }

      // Rotate hind left claws
      for(i = 0; i < 5; i++){
        updateSegment(claw[i], hindleg2LeftRotMatrix, clawMatrix[i], clawTorsoMatrix[i])
      }

      // head rotation here
        rotateY = new THREE.Matrix4().set(Math.cos(-p/2.5), 0, Math.sin(-p/2.5), 0,
                                      0, 1, 0, 0,
                                      -Math.sin(-p/2.5), 0, Math.cos(-p/2.5), 0,
                                      0, 0, 0, 1);
        rotateY_half = new THREE.Matrix4().set(Math.cos(-p/5), 0, Math.sin(-p/5), 0,
                                      0, 1, 0, 0,
                                      -Math.sin(-p/5), 0, Math.cos(-p/5), 0,
                                      0, 0, 0, 1);

      // head rotates left or right
      updateHead(rotateY);

      // shoulders rotate half the amount of the head
      var shoulderRotMatrix = new THREE.Matrix4().multiplyMatrices(rotateY_half,shoulderTorsoMatrix);
      shoulder.setMatrix(shoulderRotMatrix);

      // back rotates left or right
      var back0RotMatrix = new THREE.Matrix4().multiplyMatrices(rotateY_half,back0TorsoMatrix);
      back0.setMatrix(back0RotMatrix);
      var back1RotMatrix = new THREE.Matrix4().multiplyMatrices(back0RotMatrix,back1Matrix);
      back1.setMatrix(back1RotMatrix);
      var back2RotMatrix = new THREE.Matrix4().multiplyMatrices(back1RotMatrix,back2Matrix);
      back2.setMatrix(back2RotMatrix);

      // tail rotate more than the back (scale accordingly)
      var tailRotMatrix = new THREE.Matrix4().multiplyMatrices(rotateY,tailTorsoMatrix);
      tail.setMatrix(tailRotMatrix);

      fanNose(p);

      break;

      case(key == "S" && swim_phase == 2): // case where 2nd time S is pressed
      var time = clock.getElapsedTime(); // t seconds passed since the clock started.

      if (time > time_end){
        p = p1;
        animate = false;
        break;
      }

      if (animate_flag){
          p = (p1 - p0)*((time-time_start)/time_length) + p0; // current frame
      } else {
          p = p1;
      }

      var rotateZ0 = new THREE.Matrix4().set(1,        0,         0,        0,
                                            0, Math.cos(p/2.5), -Math.sin(p/2.5), 0,
                                            0, Math.sin(p/2.5), Math.cos(p/2.5), 0,
                                            0,        0,         0,        1);
      var rotateZ1 = new THREE.Matrix4().set(1,        0,         0,        0,
                                            0, Math.cos(p*2), Math.sin(p*2), 0,
                                            0, -Math.sin(p*2), Math.cos(p*2), 0,
                                            0,        0,         0,        1);
      var rotateZ2 = new THREE.Matrix4().set(1,        0,         0,        0,
                                            0, Math.cos(-p/5), -Math.sin(-p/5), 0,
                                            0, Math.sin(-p/5), Math.cos(-p/5), 0,
                                            0,        0,         0,        1);
      // FRONT LEG ROTATION
      var frontleg0LeftRotMatrix = new THREE.Matrix4().multiplyMatrices(rotateZ2, frontleg0LeftTorsoMatrix);
      frontleg0Left.setMatrix(frontleg0LeftRotMatrix);
      var hindleg0RightRotMatrix = new THREE.Matrix4().multiplyMatrices(rotateZ0, hindleg0RightTorsoMatrix);
      hindleg0Right.setMatrix(hindleg0RightRotMatrix);
      var tempRotMatrix = new THREE.Matrix4().multiplyMatrices(rotateZ1, frontleg1LeftMatrix);
      var frontleg1LeftRotMatrix = new THREE.Matrix4().multiplyMatrices(frontleg0LeftRotMatrix,frontleg1LeftMatrix);
      frontleg1Left.setMatrix(frontleg1LeftRotMatrix);
      tempRotMatrix = new THREE.Matrix4().multiplyMatrices(rotateZ1, hindleg1RightMatrix);
      var hindleg1RightRotMatrix = new THREE.Matrix4().multiplyMatrices(hindleg0RightRotMatrix,hindleg1RightMatrix);
      hindleg1Right.setMatrix(hindleg1RightRotMatrix);

      // all claws connected to leg2, rotate this instead of each individual claw
      var frontleg2LeftRotMatrix = new THREE.Matrix4().multiplyMatrices(frontleg1LeftRotMatrix,frontleg2LeftMatrix);
      frontleg2Left.setMatrix(frontleg2LeftRotMatrix);
      var hindleg2RightRotMatrix = new THREE.Matrix4().multiplyMatrices(hindleg1RightRotMatrix,hindleg2RightMatrix);
      hindleg2Right.setMatrix(hindleg2RightRotMatrix);

      // Rotate front left claws
      for(i = 10; i < 15; i++){
        updateSegment(claw[i], frontleg2LeftRotMatrix, clawMatrix[i], clawTorsoMatrix[i])
      }

      // Rotate hind right claws
      for(i = 5; i < 10; i++){
        updateSegment(claw[i], hindleg2RightRotMatrix, clawMatrix[i], clawTorsoMatrix[i])
      }      

      if (animate_flag){
          p = (p0 - p1)*((time-time_start)/time_length) + p1; // current frame
      } else {
          p = p0;
      }

      var rotateZ0 = new THREE.Matrix4().set(1,        0,         0,        0,
                                            0, Math.cos(-p/5), -Math.sin(-p/5), 0,
                                            0, Math.sin(-p/5), Math.cos(-p/5), 0,
                                            0,        0,         0,        1);
      var rotateZ1 = new THREE.Matrix4().set(1,        0,         0,        0,
                                            0, Math.cos(-p*2), Math.sin(-p*2), 0,
                                            0, -Math.sin(-p*2), Math.cos(-p*2), 0,
                                            0,        0,         0,        1);
      var rotateZ2 = new THREE.Matrix4().set(1,        0,         0,        0,
                                            0, Math.cos(p/2.5), -Math.sin(p/2.5), 0,
                                            0, Math.sin(p/2.5), Math.cos(p/2.5), 0,
                                            0,        0,         0,        1);
      // FRONT LEG ROTATION
      var hindleg0LeftRotMatrix = new THREE.Matrix4().multiplyMatrices(rotateZ2, hindleg0LeftTorsoMatrix);
      hindleg0Left.setMatrix(hindleg0LeftRotMatrix);
      var frontleg0RightRotMatrix = new THREE.Matrix4().multiplyMatrices(rotateZ0, frontleg0RightTorsoMatrix);
      frontleg0Right.setMatrix(frontleg0RightRotMatrix);
      var tempRotMatrix = new THREE.Matrix4().multiplyMatrices(rotateZ1, hindleg1LeftMatrix);
      var hindleg1LeftRotMatrix = new THREE.Matrix4().multiplyMatrices(hindleg0LeftRotMatrix,hindleg1LeftMatrix);
      hindleg1Left.setMatrix(hindleg1LeftRotMatrix);
      tempRotMatrix = new THREE.Matrix4().multiplyMatrices(rotateZ1, frontleg1RightMatrix);
      var frontleg1RightRotMatrix = new THREE.Matrix4().multiplyMatrices(frontleg0RightRotMatrix,frontleg1RightMatrix);
      frontleg1Right.setMatrix(frontleg1RightRotMatrix);

      // all claws connected to leg2, rotate this instead of each individual claw
      var hindleg2LeftRotMatrix = new THREE.Matrix4().multiplyMatrices(hindleg1LeftRotMatrix,hindleg2LeftMatrix);
      hindleg2Left.setMatrix(hindleg2LeftRotMatrix);
      var frontleg2RightRotMatrix = new THREE.Matrix4().multiplyMatrices(frontleg1RightRotMatrix,frontleg2RightMatrix);
      frontleg2Right.setMatrix(frontleg2RightRotMatrix);

      // Rotate hind left claws
      for(i = 0; i < 5; i++){
        updateSegment(claw[i], hindleg2LeftRotMatrix, clawMatrix[i], clawTorsoMatrix[i])
      }

      // Rotate front right claws
      for(i = 15; i < 20; i++){
        updateSegment(claw[i], frontleg2RightRotMatrix, clawMatrix[i], clawTorsoMatrix[i])
      }

      if (animate_flag){
          p = -p1 + (2 * (p1 - p0)*((time-time_start)/time_length) + p0); // current frame
      } else {
          p = p1;
      }
      rotateY = new THREE.Matrix4().set(Math.cos(-p/2.5), 0, -Math.sin(-p/2.5), 0,
                                        0, 1, 0, 0,
                                        Math.sin(-p/2.5), 0, Math.cos(-p/2.5), 0,
                                        0, 0, 0, 1);
      rotateY_half = new THREE.Matrix4().set(Math.cos(-p/5), 0, -Math.sin(-p/5), 0,
                                             0, 1, 0, 0,
                                             Math.sin(-p/5), 0, Math.cos(-p/5), 0,
                                             0, 0, 0, 1);

      // head rotates left or right
      updateHead(rotateY);

      // shoulders rotate half the amount of the head
      var shoulderRotMatrix = new THREE.Matrix4().multiplyMatrices(rotateY_half,shoulderTorsoMatrix);
      shoulder.setMatrix(shoulderRotMatrix);
      // tail rotation here
      // back rotates left or right
      var back0RotMatrix = new THREE.Matrix4().multiplyMatrices(rotateY_half,back0TorsoMatrix);
      back0.setMatrix(back0RotMatrix);
      var back1RotMatrix = new THREE.Matrix4().multiplyMatrices(back0RotMatrix,back1Matrix);
      back1.setMatrix(back1RotMatrix);
      var back2RotMatrix = new THREE.Matrix4().multiplyMatrices(back1RotMatrix,back2Matrix);
      back2.setMatrix(back2RotMatrix);
      // tail rotate more than the back (scale accordingly)
      var tailRotMatrix = new THREE.Matrix4().multiplyMatrices(rotateY,tailTorsoMatrix);
      tail.setMatrix(tailRotMatrix);

      p = p1;
      fanNose(p);

      break;

      case(key == "S" && swim_phase == 0):
      var time = clock.getElapsedTime(); // t seconds passed since the clock started.

      if (time > time_end){
        p = p1;
        animate = false;
        break;
      }

      if (animate_flag){
          p = (p1 - p0)*((time-time_start)/time_length) + p0; // current frame
      } else {
          p = p1;
      }

      var rotateZ0 = new THREE.Matrix4().set(1,        0,         0,        0,
                                            0, Math.cos(p/2.5), -Math.sin(p/2.5), 0,
                                            0, Math.sin(p/2.5), Math.cos(p/2.5), 0,
                                            0,        0,         0,        1);
      var rotateZ1 = new THREE.Matrix4().set(1,        0,         0,        0,
                                            0, Math.cos(p*2), Math.sin(p*2), 0,
                                            0, -Math.sin(p*2), Math.cos(p*2), 0,
                                            0,        0,         0,        1);
      var rotateZ2 = new THREE.Matrix4().set(1,        0,         0,        0,
                                            0, Math.cos(-p/5), -Math.sin(-p/5), 0,
                                            0, Math.sin(-p/5), Math.cos(-p/5), 0,
                                            0,        0,         0,        1);
      // FRONT LEG ROTATION
      var frontleg0LeftRotMatrix = new THREE.Matrix4().multiplyMatrices(rotateZ2, frontleg0LeftTorsoMatrix);
      frontleg0Left.setMatrix(frontleg0LeftRotMatrix);
      var hindleg0RightRotMatrix = new THREE.Matrix4().multiplyMatrices(rotateZ0, hindleg0RightTorsoMatrix);
      hindleg0Right.setMatrix(hindleg0RightRotMatrix);
      var tempRotMatrix = new THREE.Matrix4().multiplyMatrices(rotateZ1, frontleg1LeftMatrix);
      var frontleg1LeftRotMatrix = new THREE.Matrix4().multiplyMatrices(frontleg0LeftRotMatrix,frontleg1LeftMatrix);
      frontleg1Left.setMatrix(frontleg1LeftRotMatrix);
      tempRotMatrix = new THREE.Matrix4().multiplyMatrices(rotateZ1, hindleg1RightMatrix);
      var hindleg1RightRotMatrix = new THREE.Matrix4().multiplyMatrices(hindleg0RightRotMatrix,hindleg1RightMatrix);
      hindleg1Right.setMatrix(hindleg1RightRotMatrix);

      // All claws connected to leg2, rotate this instead of each individual claw
      var frontleg2LeftRotMatrix = new THREE.Matrix4().multiplyMatrices(frontleg1LeftRotMatrix,frontleg2LeftMatrix);
      frontleg2Left.setMatrix(frontleg2LeftRotMatrix);
      var hindleg2RightRotMatrix = new THREE.Matrix4().multiplyMatrices(hindleg1RightRotMatrix,hindleg2RightMatrix);
      hindleg2Right.setMatrix(hindleg2RightRotMatrix);

      // Rotate front left claws
      for(i = 10; i < 15; i++){
        updateSegment(claw[i], frontleg2LeftRotMatrix, clawMatrix[i], clawTorsoMatrix[i])
      }

      // Rotate hind right claws
      for(i = 5; i < 10; i++){
        updateSegment(claw[i], hindleg2RightRotMatrix, clawMatrix[i], clawTorsoMatrix[i])
      }

      rotateY = new THREE.Matrix4().set(Math.cos(-p/2.5), 0, -Math.sin(-p/2.5), 0,
                                        0, 1, 0, 0,
                                        Math.sin(-p/2.5), 0, Math.cos(-p/2.5), 0,
                                        0, 0, 0, 1);
      rotateY_half = new THREE.Matrix4().set(Math.cos(-p/5), 0, -Math.sin(-p/5), 0,
                                             0, 1, 0, 0,
                                             Math.sin(-p/5), 0, Math.cos(-p/5), 0,
                                             0, 0, 0, 1);

      updateHead(rotateY);

      fanNose(p);

      // shoulders rotate half the amount of the head
      var shoulderRotMatrix = new THREE.Matrix4().multiplyMatrices(rotateY_half,shoulderTorsoMatrix);
      shoulder.setMatrix(shoulderRotMatrix);

      // back rotates left or right
      var back0RotMatrix = new THREE.Matrix4().multiplyMatrices(rotateY_half,back0TorsoMatrix);
      back0.setMatrix(back0RotMatrix);
      var back1RotMatrix = new THREE.Matrix4().multiplyMatrices(back0RotMatrix,back1Matrix);
      back1.setMatrix(back1RotMatrix);
      var back2RotMatrix = new THREE.Matrix4().multiplyMatrices(back1RotMatrix,back2Matrix);
      back2.setMatrix(back2RotMatrix);
      // tail rotate more than the back (scale accordingly)
      var tailRotMatrix = new THREE.Matrix4().multiplyMatrices(rotateY,tailTorsoMatrix);
      tail.setMatrix(tailRotMatrix);
      break;

      // fan nose appendages
      case(key == "N"):
      var time = clock.getElapsedTime(); // t seconds passed since the clock started.

      if (time > time_end){
        p = p1;
        animate = false;
        break;
      }

      if (animate_flag){
          p = (p1 - p0)*((time-time_start)/time_length) + p0; // current frame
      } else {
          p = p1;
      }

      fanNose(p);

      break;

      default:
      break;
  }
}

if(newNosePosition.length > 0){
  noseMatrix = newNosePosition;
}

// LISTEN TO KEYBOARD
// Hint: Pay careful attention to how the keys already specified work!
var keyboard = new THREEx.KeyboardState();
var grid_state = false;
var key;
keyboard.domElement.addEventListener('keydown',function(event){
  if (event.repeat)
    return;
  if(keyboard.eventMatches(event,"Z")){  // Z: Reveal/Hide helper grid
    grid_state = !grid_state;
    grid_state? scene.add(grid) : scene.remove(grid);}
  else if(keyboard.eventMatches(event,"0")){    // 0: Set camera to neutral position, view reset
    camera.position.set(45,0,0);
    camera.lookAt(scene.position);}
  else if(keyboard.eventMatches(event,"U")){
    (key == "U")? init_animation(p1,p0,time_length) : (init_animation(0,Math.PI/4,1), key = "U")}


  // TO-DO: BIND KEYS TO YOUR JUMP CUTS AND ANIMATIONS
  // Note: Remember spacebar sets jumpcut/animate!
  // Hint: Look up "threex.keyboardstate by Jerome Tienne" for more info.

  else if(keyboard.eventMatches(event,"E")){
    (key == "E")? init_animation(p1,p0,time_length) : (init_animation(0,Math.PI/4,1), key = "E")}
  else if(keyboard.eventMatches(event,"H")){
    (key == "H")? init_animation(p1,p0,time_length) : (init_animation(0,Math.PI/10,1), key = "H")}
  else if(keyboard.eventMatches(event,"G")){
    (key == "G")? init_animation(p1,p0,time_length) : (init_animation(0,Math.PI/10,1), key = "G")}
  else if(keyboard.eventMatches(event,"T")){
    (key == "T")? init_animation(p1,p0,time_length) : (init_animation(0,Math.PI/20,1), key = "T")}
  else if(keyboard.eventMatches(event,"V")){
    (key == "V")? init_animation(p1,p0,time_length) : (init_animation(0,Math.PI/20,1), key = "V")}
  else if(keyboard.eventMatches(event,"D")){
    (key == "D")? init_animation(p1,p0,time_length) : (init_animation(0,Math.PI/20,1), key = "D")}
  else if(keyboard.eventMatches(event,"S")){
      if (key == "S" && swim_phase == 2) {
          // return to original position (finished 2 motions)
          swim_phase = 0;
          init_animation(p1,p0,time_length);
      } else if (key == "S" && swim_phase == 1) {
          // key S has been pressed once before, go through second motion
          swim_phase = 2;
          init_animation(0,Math.PI/4, 1);
      } else {
          // first time key S is being pressed
          swim_phase = 1;
          key = "S";
          init_animation(0,Math.PI/4, 1);
      }}
  else if(keyboard.eventMatches(event,"N")){
    (key == "N")? init_animation(p1,p0,time_length) : (init_animation(0,Math.PI/4,1), key = "N")}
  else if(keyboard.eventMatches(event,"space")){
    animate_flag = !animate_flag;}


    });

// SETUP UPDATE CALL-BACK
// Hint: It is useful to understand what is being updated here, the effect, and why.
function update() {
  updateBody();

  requestAnimationFrame(update);
  renderer.render(scene,camera);
}

// Fan entire nose
function fanNose(p){
  var rotateZUp = new THREE.Matrix4().set(1,        0,         0,        0,
                                        0, Math.cos(-p), -Math.sin(-p), 0,
                                        0, Math.sin(-p), Math.cos(-p), 0,
                                        0,        0,         0,        1);
  var rotateZDown = new THREE.Matrix4().set(1,        0,         0,        0,
                                        0, Math.cos(-p), Math.sin(-p), 0,
                                        0, -Math.sin(-p), Math.cos(-p), 0,
                                        0,        0,         0,        1);
  var rotateYRight = new THREE.Matrix4().set(Math.cos(-p/2.5), 0, Math.sin(-p/2.5), 0,
                                    0, 1, 0, 0,
                                    -Math.sin(-p/2.5), 0, Math.cos(-p/2.5), 0,
                                    0, 0, 0, 1);
  var rotateYLeft = new THREE.Matrix4().set(Math.cos(-p/2.5), 0, -Math.sin(-p/2.5), 0,
                                    0, 1, 0, 0,
                                    Math.sin(-p/2.5), 0, Math.cos(-p/2.5), 0,
                                    0, 0, 0, 1);


  var duplicateNoseTorsoMatrix = noseTorsoMatrix;
  // Fan top nose segments
  for(k = 0; k < 6; k++){
    fanNoseSegment(nose[k], rotateZUp, noseMatrix[k], noseTorsoMatrix[k]);
  }

  // Fan bottom nose segments
  for(k = 6; k < 10; k++){
    fanNoseSegment(nose[k], rotateZDown, noseMatrix[k], noseTorsoMatrix[k]);
  }

  // Fan right nose segments
  for(k = 11; k < 17; k++){
    fanNoseSegment(nose[k], rotateYRight, noseMatrix[k], noseTorsoMatrix[k]);
  }

  // Fan unorganized loner nose segment
  fanNoseSegment(nose[10], rotateYLeft, noseMatrix[k], noseTorsoMatrix[10]);

  // Fan left nose segments
  for(k = 17; k < 22; k++){
    fanNoseSegment(nose[k], rotateYLeft, noseMatrix[k], noseTorsoMatrix[k]);
  }
}

// Fan individual nose segment
function fanNoseSegment(noseSegment, rotation, noseMatrix, noseTorsoMatrix) {

  // Get inverse of parent of fan segment so that we can get back original segment matrix
  inverseHead3TorsoMatrix = new THREE.Matrix4();
  inverseHead3TorsoMatrix.getInverse(head3TorsoMatrix);

  var noseRotMatrix0 = new THREE.Matrix4().multiplyMatrices(noseTorsoMatrix, rotation);
  var noseRotMatrix1 = new THREE.Matrix4().multiplyMatrices(inverseHead3TorsoMatrix, noseRotMatrix0);

  noseSegment.setMatrix(noseRotMatrix0);
}

// Apply a transformation to a segment
function updateSegment(segment, rotation, segmentMatrix, segmentTorsoMatrix) {

  var transMatrix = new THREE.Matrix4().multiplyMatrices(rotation,segmentMatrix);
  segmentTorsoMatrix.copy(transMatrix);

  segment.setMatrix(segmentTorsoMatrix);
}

// Apply transformation to head
function updateHead(rotation) {

  // Rotate all head segments
  var head0RotMatrix = new THREE.Matrix4().multiplyMatrices(rotation,head0TorsoMatrix);
  head0.setMatrix(head0RotMatrix);
  //head0TorsoMatrix.copy(head0RotMatrix);

  var head1RotMatrix = new THREE.Matrix4().multiplyMatrices(head0RotMatrix,head1Matrix);
  head1.setMatrix(head1RotMatrix);
  //head1Matrix.copy(head1RotMatrix);

  var head2RotMatrix = new THREE.Matrix4().multiplyMatrices(head1RotMatrix,head2Matrix);
  head2.setMatrix(head2RotMatrix);
  //head2Matrix.copy(head2RotMatrix);


  var head3RotMatrix = new THREE.Matrix4().multiplyMatrices(head2RotMatrix,head3Matrix);
  head3.setMatrix(head3RotMatrix);
  //head3Matrix.copy(head3RotMatrix);

  // Rotate nose
  for(i = 0; i < 22; i++){
    updateSegment(nose[i], head3RotMatrix, noseMatrix[i], noseTorsoMatrix[i])
  }

}

update();
